package br.com.basecode.jpa.entity;

import java.io.Serializable;

public interface Entity extends Serializable {

	Serializable getId();

	void setId(Serializable id);

}
